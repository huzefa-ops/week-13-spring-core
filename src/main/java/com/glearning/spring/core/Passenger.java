package com.glearning.spring.core;

public class Passenger {
	
	//coupling
	private UberGo driver;
	
	public Passenger(UberGo driver) {
		this.driver = driver;
	}
	
	public void commute(String source, String destination) {
		driver.trip(source, destination);
	}

}
