package com.glearning.spring.core;

public class UberPrimeDriver implements UberGo, UberPrime{

	public void primeRide(String source, String destination) {
		System.out.println("Riding from source "+ source + " to destination "+ destination+ " with Uber Prime");
	}

	public void trip(String source, String destination) {
		System.out.println("Riding from source "+ source + " to destination "+ destination+ " with Uber Go");
		System.out.println("Enjoy Amazon Prime music while on the ride !!");
	}

}
