package com.glearning.spring.core;

public class UberGoDriver implements UberGo {
	
	public void trip(String source, String destination) {
		System.out.println("travelling from "+source+ " to "+ destination+ " by UberGo!!");
	}

}
