package com.glearning.spring.core;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Client {
	
	public static void main(String[] args) {
		//client - dependency management is done by the client
		/*
		 * UberGoDriver hari = new UberGoDriver();
		 * 
		 * Passenger ravi = new Passenger(hari);
		 */
		//spring will scan the file and start creating the object.
		// After the object is created, the bean will be registered inside the application context
		// We can request the bean from the application context with the bean name.
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("application-context.xml");
		Passenger ravi = applicationContext.getBean("passenger", Passenger.class);
		ravi.commute("BTM-2nd Cross", "BIAL Airport");
	}

}
